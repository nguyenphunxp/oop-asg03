import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by NguyenPhu on 10/7/2016.             
 */
class node {
    int data;
    node next;
    public node(int x) {
        data = x;
        next = null;
    }
}

class stack {
    node top = null;
    public boolean isEmpty() {
        if (top == null)
            return true;
        else
            return false;
    }
    public void push(int x) {
        if(isEmpty() == true) {
            node ph = new node(x);
            top = ph;
        }
        else {
            node ph = new node(x);
            ph.next = top;
            top = ph;
        }
    }
    public int pop() {
        if(isEmpty() == true) {
            System.out.println("NGAN XEP RONG");
            return -1;
        }
        else {
            int x = top.data;
            top = top.next;
            return x;
        }
    }

    public void display() {
        if(isEmpty() == true) {
            System.out.println("KHONG CO PHAN TU TRONG NGAN XEP");
        }
        else {
            node current = top;
            while(current != null) {
                System.out.println(current.data);
                current = current.next;
            }
        }
    }
}

public class LinkedStack {
    public static void main(String[] args) throws IOException {
        BufferedReader obj=new BufferedReader(new InputStreamReader(System.in));
        int ch,temp;
        stack sl=new stack();
        do {
            System.out.println("1.Chen 1 phan tu trong ngap xep");
            System.out.println("2.Xoa bo 1 phan tu");
            System.out.println("3.Danh sach phan tu hien co");
            System.out.println("4.Thoat");
            System.out.println("Lua chon!!!");
            ch=Integer.parseInt(obj.readLine());
            switch (ch) {
                case 1:System.out.println("Nhap phan tu de duoc chen: ");
                    temp=Integer.parseInt(obj.readLine());
                    sl.push(temp);
                    break;
                case 2:temp=sl.pop();
                    System.out.println("Phan tu xoa bo la: "+temp);
                    break;
                case 3:sl.display();
            }
        }
        while(ch != 4);
    }
}
